from tkinter import *
from PIL import Image, ImageTk
import merge
import json

def saveConfig():
    # Save configuration
    config["max_ratio"] = varMaxRatio.get()
    config["min_ratio"] = varMinRatio.get()
    config["width"] = varWidth.get()
    config["height"] = varHeight.get()
    config["ratio"] = varStartRatio.get()
    config["random"] = varRandom.get()

    configFile = open("config.json", "w")
    json.dump(config, configFile)

def startMerge():
    saveConfig()
    im = merge.main()
    h = int(im.size[1] * (1280 / im.size[0]))
    im = im.resize((1280, h))

    global mergedImageWindow
    global scrollBarX
    global scrollBarY
    global tkimage
    global canvas

    mergedImageWindow = Toplevel(app, width=1280, height=h)

    tkimage = ImageTk.PhotoImage(image=im)
    
    canvas = Canvas(mergedImageWindow, width=1280, height=h, scrollregion=(0, 0, 1280, h))

    scrollBarY = Scrollbar(mergedImageWindow,orient="vertical", command=canvas.yview)
    scrollBarX = Scrollbar(mergedImageWindow, orient="horizontal", command=canvas.xview)
    
    scrollBarX.pack(side="bottom", fill="x")
    scrollBarY.pack(side="right", fill="y")

    canvas.config(xscrollcommand=scrollBarX.set, yscrollcommand=scrollBarY.set)
    canvas.pack(fill="both", expand=True)
    canvas.create_image(0, 0, image=tkimage, anchor=NW)
    

if __name__ == "__main__":
    configFile = open("config.json")
    config = json.load(configFile)
    configFile.close()

    app = Tk()

    varMaxRatio = DoubleVar()
    varMaxRatio.set(config["max_ratio"])
    varMinRatio = DoubleVar()
    varMinRatio.set(config["min_ratio"])
    varStartRatio = DoubleVar()
    varStartRatio.set(config["ratio"])
    varWidth = IntVar()
    varWidth.set(config["width"])
    varHeight = IntVar()
    varHeight.set(config["height"])

    varRandom = BooleanVar()
    varRandom.set(config["random"])

    # Random ?
    randomButon = Checkbutton(app, text="Random", variable=varRandom)
    randomButon.pack()

    # Image scale before the algorithm
    scaleStartRatio = Scale(app, label="Start ratio", from_=0.1, to=2.0, resolution=0.05, orient=HORIZONTAL, variable=varStartRatio, length=400)
    scaleStartRatio.pack()

    # Width of the output image
    scaleWidth = Scale(app, label="Width merged", from_=100, to=24000, resolution=100, orient=HORIZONTAL, variable=varWidth, length=400)
    scaleWidth.pack()

    # Height of the output image
    scaleHeight = Scale(app, label="Height merged", from_=100, to=24000, resolution=100, orient=HORIZONTAL, variable=varHeight, length=400)
    scaleHeight.pack()

    # Max ratio of the pictures
    scaleMaxRatio = Scale(app, label="Maximum ratio", from_=1.0, to=2.0, resolution=0.05, orient=HORIZONTAL, variable=varMaxRatio, length=400)
    scaleMaxRatio.pack()

    # Min ratio of the pictures
    scaleMinRatio = Scale(app, label="Minimum ratio", from_=0.1, to=1.0, resolution=0.05, orient=HORIZONTAL, variable=varMinRatio, length=400)
    scaleMinRatio.pack()

    startButton = Button(app, text="Merge", command=startMerge)
    startButton.pack()

    app.mainloop()


