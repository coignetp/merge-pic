from PIL import Image
from random import shuffle
import os
import json
import copy

logFile = open("log.txt", "w")

def getKey(elt):
    return elt["image"].size[1] * elt["image"].size[0]

def nextBorderWide(size, placements: list, grid: list, gridLimit: list, c0: int, l0: int) -> int:
    i = 1

    while gridLimit[0][c0+i] - gridLimit[0][c0] < size[0]:
        i += 1
        if c0 + i >= len(gridLimit[0]):
            return -1

    logFile.write(f"Grid W limit {gridLimit[0][c0+i] - gridLimit[0][c0]} / {size} --> {c0 + i}\n")
    return c0 + i

def nextBorderHeight(size, placements: list, grid: list, gridLimit: list, c0: int, l0: int) -> int:
    i = 1

    while gridLimit[1][l0+i] - gridLimit[1][l0] < size[1]:
        i += 1
        if l0 + i >= len(gridLimit[1]):
            return -1

    logFile.write(f"Grid H limit {gridLimit[1][l0+i] - gridLimit[1][l0]} / {size} --> {l0 + i}\n")
    return l0 + i

def getMaxMinSize(size, placements: list, grid: list, gridLimit: list, c0: int, l0: int):
    def updateSize(size, placements: list, grid: list, gridLimit: list, c0: int, l0: int, c: int, l: int, sizeAfter):
        # Update the ratio according to the closest border
        if (gridLimit[0][c + 1] - gridLimit[0][c0]) / size[0] < (gridLimit[1][l + 1] - gridLimit[1][l0]) / size[1]:
            ratio = (gridLimit[0][c + 1] - gridLimit[0][c0]) / size[0]
            sizeBefore = sizeAfter
            sizeAfter = (gridLimit[0][c + 1] - gridLimit[0][c0], int(size[1] * ratio))
            c += 1
        else: 
            ratio = (gridLimit[1][l + 1] - gridLimit[1][l0]) / size[1]
            sizeBefore = sizeAfter
            sizeAfter = (int(size[0] * ratio), gridLimit[1][l + 1] - gridLimit[1][l0])
            l += 1

        return ratio, c, l, sizeBefore, sizeAfter

    ratio = 1.0
    c = c0
    l = l0
    sizeAfter = size
    sizeBefore = size

    ratio, c, l, sizeBefore, sizeAfter = updateSize(size, placements, grid, gridLimit, c0, l0, c, l, sizeAfter)

    logFile.write(f"This ratio {ratio} for {c0}:{c} | {l0}:{l}\n")

    while isRectFree(sizeAfter, placements, gridLimit[0][c0], gridLimit[1][l0], gridLimit[0][-1], gridLimit[1][-1], False):
        if c == len(gridLimit[0]) - 1 or l == len(gridLimit[1]) - 1:
            logFile.write(f"getMaxMinSize: limit {c} {l}\n")
            return sizeAfter
        
        ratio, c, l, sizeBefore, sizeAfter = updateSize(size, placements, grid, gridLimit, c0, l0, c, l, sizeAfter)

        logFile.write(f"This ratio {ratio} for {c0}:{c} | {l0}:{l}\n")
    
    logFile.write(f"Space not free anymore {c0}:{c} | {l0}:{l} -> {size}:{sizeBefore}:{sizeAfter} ({isRectFree(sizeAfter, placements, gridLimit[0][c0], gridLimit[1][l0], gridLimit[0][-1], gridLimit[1][-1], False)})\n")

    return sizeBefore
 
def getBigRatio(size, placements: list, grid: list, gridLimits: list, c: int, l: int) -> float:
    # If the cell is free, then place the picture
    w = nextBorderWide(size, placements, grid, gridLimits, c, l)
    h = nextBorderHeight(size, placements, grid, gridLimits, c, l)

    ratio = 1.0

    if w != -1:
        ratio = (gridLimits[0][w] - gridLimits[0][c]) / size[0]
        if not isRectFree((int(size[0] * ratio), int(size[1] * ratio)), placements, gridLimits[0][c], gridLimits[1][l], gridLimits[0][-1], gridLimits[1][-1], True):
            ratio = 1.0

    if h != -1:
        ratio = min(ratio, (gridLimits[1][h] - gridLimits[1][l]) / size[1])
        if not isRectFree((int(size[0] * ratio), int(size[1] * ratio)), placements, gridLimits[0][c], gridLimits[1][l], gridLimits[0][-1], gridLimits[1][-1], True):
            ratio = 1.0

    logFile.write(f"---> w {w}:{c}/ h {h}:{l} # {ratio}\n")

    return ratio

def getPictures(ratio: float):
    """
    Load all the pictures in the folder
    """
    path = "pictures"

    pics = [{"path": f, "image": Image.open(os.path.join(path, f))} for f in os.listdir(path) if os.path.isfile(os.path.join(path, f)) and f[:-5] != ".gitkeep"[:-5]]

    pics.sort(key=getKey)

    if ratio > 0 and ratio != 1.0:
        for i in range(0, len(pics)):
            pics[i]["image"] = pics[i]["image"].resize((int(pics[i]["image"].size[0] * ratio), int(pics[i]["image"].size[1] * ratio)), Image.ANTIALIAS)

    return pics

def isRectFree(size, placements, x, y, maxWidth, maxHeight, canBeOutside) -> bool:
    """
    Return True if there is enough space left from (x,y)
    """
    for p in placements:
        if ((x < p["x"] + p["w"]
                and y < p["y"] + p["h"]
                and x + size[0] > p["x"]
                and y + size[1] > p["y"])
                or ((x + size[0] > maxWidth and not canBeOutside) or x >= maxWidth)
                or ((y + size[1] > maxHeight and not canBeOutside) or y >= maxHeight)):
            return False

    return True

def placePictures(pictures: list, width: int, height: int, minRatio: float, maxRatio: float, minSize: int):
    """
    Algorithm for placing the pictures.
    Inspired from https://www.codeproject.com/Articles/210979/Fast-optimizing-rectangle-packing-algorithm-for-bu
    """
    placements = []
    gridLimits = [[0, width], [0, height]]
    grid = [[False]]

    for i in range(len(pictures) - 1, -1, -1):
        pic = pictures[i]
        placed = False
        logFile.write("\n\n")
        logFile.write(f"{grid}")
        logFile.write(f"{gridLimits}\n")
        
        # We try to find a free cell for each picture
        for l in range(0, len(grid)):
            if placed:
                break
            for c in range(0, len(grid[0])):
                if placed:
                    break
                ratio = 1.0

                # If the cell is free
                if grid[l][c] == False:
                    # Picture fully placed (ratio=1)
                    if isRectFree(pic["image"].size, placements, gridLimits[0][c], gridLimits[1][l], width, height, False):
                        # If the border of the picture is closed to a cell border, we try to resize the picture in order to
                        # complete the cell
                        ratio = getBigRatio(pic["image"].size, placements, grid, gridLimits, c, l)

                        if ratio > maxRatio:
                            logFile.write(f"Ratio too big {ratio}/{maxRatio}\n")
                            ratio = 1.0

                        if ratio != 1.0:
                            logFile.write(f"resizing big to {ratio}\n")
                            pic["image"] = pic["image"].resize((int(pic["image"].size[0] * ratio), int(pic["image"].size[1] * ratio)), Image.ANTIALIAS)

                        placements.append({"id": i, "x": gridLimits[0][c], "y":  gridLimits[1][l], "w": pic["image"].size[0], "h": pic["image"].size[1]})
                        placed = True

                        grid = updateGrid(grid, gridLimits, placements, c, l, pic["image"].size[0], pic["image"].size[1])
                    else:
                        # If there is not enough place for the full picture, then we try to resize it
                        sizeAfter = getMaxMinSize(pic["image"].size, placements, grid, gridLimits, c, l)
                        ratio = sizeAfter[0]/pic["image"].size[0]

                        if ratio < 1.0 and ratio >= minRatio:
                            logFile.write(f"resizing little with {ratio}\n")
                            pic["image"] = pic["image"].resize(sizeAfter, Image.ANTIALIAS)
                            pictures[i] = pic

                            placements.append({"id": i, "x": gridLimits[0][c], "y":  gridLimits[1][l], "w": pic["image"].size[0], "h": pic["image"].size[1]})
                            placed = True

                            grid = updateGrid(grid, gridLimits, placements, c, l, pic["image"].size[0], pic["image"].size[1])
                else:
                    logFile.write(f"Position {c}:{l} not free\n")

                if placed:
                    logFile.write(f"Picture {i} placed at (c:{c}, l:{l}) with ratio {ratio}\n")

    return placements



def updateGrid(grid: list, gridLimits: list, placements: list, c: int, l: int, cellWidth: int, cellHeight: int) -> list:
    addedCol = 0
    addedRow = 0

    # Update the columns of the grid
    for i in range(c, len(gridLimits[0])):
        if gridLimits[0][i] > gridLimits[0][c] + cellWidth:
            gridLimits[0].insert(i, gridLimits[0][c] + cellWidth)
            addedCol = 1
            break

        elif gridLimits[0][i] == gridLimits[0][c] + cellWidth:
            break

    # Update the rows of the grid
    for i in range(l, len(gridLimits[1])):
        if gridLimits[1][i] > gridLimits[1][l] + cellHeight:
            gridLimits[1].insert(i, gridLimits[1][l] + cellHeight)
            addedRow = 1
            break

        elif gridLimits[1][i] == gridLimits[1][l] + cellHeight:
            break

    gridBis = [[False for _ in range(len(grid[0]) + addedCol)] for _ in range(len(grid) + addedRow)]

    for p in placements:
        for i in range(len(gridLimits[0]) - 1):
            for j in range(len(gridLimits[1]) - 1):
                if (gridLimits[0][i] < p["x"] + p["w"] 
                        and gridLimits[0][i + 1] > p["x"]
                        and gridLimits[1][j] < p["y"] + p["h"]
                        and gridLimits[1][j+1] > p["y"]):
                    gridBis[j][i] = True

    return gridBis

def main():
    with open("config.json") as configFile:
        config = json.load(configFile)
        # Pixels
        mergedWidth = config["width"]
        # Pixels
        mergedHeight = config["height"]

        minSize = config["min_size"]

        # Ratio
        ratio = config["ratio"]
        minRatio = config["min_ratio"]
        maxRatio = config["max_ratio"]
        
        pictures = getPictures(ratio)

        if (config["random"]):
            shuffle(pictures)

        placements = placePictures(pictures, mergedWidth, mergedHeight, minRatio, maxRatio, minSize)

        merged = Image.new('RGBA', (mergedWidth, mergedHeight), (255, 255, 255, 255))

        logFile.write(f"\n\n{placements}")
        logFile.write(f"\n\n{pictures}")

        for p in placements:
            merged.paste(pictures[p["id"]]["image"], (p["x"], p["y"]))

        merged.save('out.bmp')

        return merged

if __name__ == "__main__":
    main()